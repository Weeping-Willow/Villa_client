/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author vitol
 */
public class Admin_user extends User{
    
    public Admin_user(String user_name, String password, Connect con) throws IOException {
        super(user_name, password, con);
    }

    

    String get_user_list() throws IOException {
        String cmd = "get_user_list \n";
        con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
        return response;
    }

    void un_block(String user) throws IOException {
        String cmd = "block_unblock_user "+user+"\n";
        con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
        InfoBox box =new InfoBox(response+" user","I'm a pickle");
    }

    void logoff() throws IOException {
        String cmd = "quit\n";
        con.serverOut.write(cmd.getBytes());
        con.close();
    }

    void delete(String user) throws IOException {
        String cmd = "delete_user "+user+"\n";
        con.serverOut.write(cmd.getBytes());
        InfoBox box =new InfoBox(user+" is deleted","I'm a pickle");
    }

    String search_user(String search) throws IOException {
        String cmd = "search_user "+search+"\n";
        con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
        return response;
    }

    ArrayList<String> get_user_log(String user) throws IOException {
        ArrayList<String> msg = new ArrayList<>();
        String cmd = "get_user_log "+user+"\n";
        con.serverOut.write(cmd.getBytes());
        while(true)
        {
            String response = con.bufferedIn.readLine();
            if(response.equalsIgnoreCase("end"))
                break;
            else
                msg.add(response);
        }
        
        return msg;
    }

    void register_this_user_ad(String f_name, String l_name, String username, String d_name, String password1, String email, int admin) throws IOException {
        String cmd = "register_user_ad "+f_name+" "+l_name+" "+username+" "+d_name+" "+password1+" "+email+" "+admin+"\n";
        con.serverOut.write(cmd.getBytes());
        InfoBox box = new InfoBox("User registered", "sd");
    }

    void update_password_from_ad(String userch, String pw) throws IOException {
         String cmd = "update_password_from_ad "+userch+" "+pw+"\n";
         con.serverOut.write(cmd.getBytes());
         InfoBox box =new InfoBox("Password changed","I'm a pickle");
    }
   
}
