/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitol
 */
public class Connect {
     private String serverName= null ;
    private int serverPort = 0;
    public Socket socket;
    public OutputStream serverOut;
    public InputStream serverIn;
    public BufferedReader bufferedIn;

    public Connect(String serverName,int serverPort) {
       this.serverName = serverName;
       this.serverPort = serverPort;
        connect();
    }
    
    
    public boolean connect()
    {
        try {
            this.socket = new Socket(serverName,serverPort);     
            this.serverOut = socket.getOutputStream();
            this.serverIn  =socket.getInputStream();
            this.bufferedIn = new BufferedReader(new InputStreamReader(serverIn));
            return true;
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    Socket getSocket() {
        return socket;
    }

    OutputStream getServerOut() {
        return serverOut;
    }

    InputStream getServerin() {
        return serverIn;
    }

    BufferedReader getBufferedIn() {
        return bufferedIn;
    }

    void close() throws IOException {
        socket.close();
        serverOut.close();
        serverIn.close();
        bufferedIn.close();
    }
    
}
