/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import javax.swing.JOptionPane;

/**
 *
 * @author vitol
 */
public class InfoBox {

    public InfoBox(String infoMessage, String titleBar) {
        infoBox(infoMessage, titleBar);
    }
    
    public static void infoBox(String infoMessage, String titleBar) //info box 
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }  
}
