/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitol
 */
public class Client_listener extends Thread{
    Connect con;
    Chat chat_gui;
    Chat_group opened_chat;
    public Client_listener(Connect con,Chat chat_gui,Chat_group opened_chat) throws IOException {
        this.con = con;
        this.chat_gui = chat_gui;
        this.opened_chat = opened_chat;
        String cmd = "add_me\n";
        con.serverOut.write(cmd.getBytes());
    }
    @Override
    public void run()
    {
        try {
                String response;
                while((response = con.bufferedIn.readLine()) != null)
                {
                    if(response.equalsIgnoreCase("update")){
                        ArrayList<String> msg = new ArrayList<>();
                        
                        String cmd = "get_previous_messages "+opened_chat.group.id+"\n";
                        con.serverOut.write(cmd.getBytes());
                        
                        while(true)
                        {
                            String responses = con.bufferedIn.readLine();
                            if(responses.equalsIgnoreCase("end"))
                                break;
                            else
                                msg.add(responses);
                        }
                        chat_gui.display_msg(msg);
                    }
                    else if(response.equalsIgnoreCase("stop"))
                    {
                        return;
                    }
                    else
                    {
                        chat_gui.display_user(response);
                        System.out.println(response);
                    }
                }
            
        } catch (IOException ex) {
            Logger.getLogger(Client_listener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
