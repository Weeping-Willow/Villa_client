/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author vitol
 */
public class Chat_group {
     Group group = null;
    Connect con= null;
    Standart_user user= null;

    public Chat_group(Group group,Connect con,Standart_user user ) {
        this.group = group;
        this.con=  con;
        this.user = user;
    }

    public ArrayList<String> getMessages() throws IOException {
        ArrayList<String> msg = new ArrayList<>();
        String cmd = "get_previous_messages "+group.id+"\n";
        con.serverOut.write(cmd.getBytes());
        while(true)
        {
            String response = con.bufferedIn.readLine();
            if(response.equalsIgnoreCase("end"))
                break;
            else
                msg.add(response);
        }
        
        return msg;
    }

    void send_msg(String msg) throws IOException {
        Time date = new Time();
        String cmd = "msg "+group.id+" "+user.getId()+" "+date.getDate()+" "+msg+"\n";
        con.serverOut.write(cmd.getBytes());
        
    }

    void removeMember(String temp) throws IOException {
        String cmd = "remove_member "+temp+"\n";
        user.con.serverOut.write(cmd.getBytes());
        group.groupMembers();
    }

    void add_friend_member() throws IOException {
        String cmd = "get_add_friend_member_list "+user.getId()+" "+group.id+"\n";
        user.con.serverOut.write(cmd.getBytes());
    }

    void addFriend(String temp) throws IOException {
        String cmd = "add_friend_to_group "+temp+" "+group.id+"\n";
        user.con.serverOut.write(cmd.getBytes());
    }
    
}
