/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JTextField;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author vitol
 */
public class User {
    public String username;
    public String password;
    
    public int accountLevel;
    private int isBlocked;
    boolean reject = false;
    public String[] tokens; 
    
    String msg;
    Connect con;
    User(String user_name,String password,Connect con) throws IOException
    {
        this.username = user_name;
        this.password = password;
        
        this.con = con;
        
    }

    User(Connect con) {
        this.con = con;
    }
    
    
    public void User_auth() throws IOException
    { 
        send_login_data();
        if (con.connect())
        {
            if(reject)
                return;
            else 
                return;
            
        }
        else
        {
            InfoBox box  =new InfoBox("Server 404", "pickle");
            box = null;
            return;
        }

    }

    private void send_login_data() {
        String cmd = "login "+ username+" "+password+"\n";
        try {
            con.serverOut.write(cmd.getBytes());
            
            String response = con.bufferedIn.readLine();
            String msg = ("Response:"+response);
            String receivedMsg = response;
            tokens = StringUtils.split(receivedMsg);
            if(tokens[0].equalsIgnoreCase("nok"))
            {
                InfoBox box  =new InfoBox("No client with such a username or password", "potatoe");
                box = null;
                return;
            }
            else
            {
                System.out.println(isBlocked);
                if(Integer.parseInt(tokens[9]) == 1)
                {
                    
                    InfoBox box  =new InfoBox("Account is blocked", "potatoe");
                    box = null;
                    return;
                }
                else
                    reject = true;

            }
            
            
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return;
    }

    public Standart_user  data_entry_st() throws IOException {
        Standart_user st_user = new Standart_user(Integer.parseInt(tokens[1]), tokens[2], tokens[3], username, tokens[5], password,tokens[7], con);
        this.username = tokens[4];
        this.password = tokens[6];
        this.accountLevel = Integer.parseInt(tokens[8]);
        this.isBlocked = Integer.parseInt(tokens[9]);
        return st_user;
    }
    public Admin_user data_entry_ad() throws IOException {
        Admin_user ad_user = new Admin_user(username, password, con);
        this.username = tokens[4];
        this.password = tokens[6];
        this.accountLevel = Integer.parseInt(tokens[8]);
        this.isBlocked = Integer.parseInt(tokens[9]);
        return ad_user;
    }
   
    public String getUsername() {
        return this.username;
    }


    public String getPassword() {
        return this.password;
    }
    public boolean is_valid_email(String email) 
    { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                            "[a-zA-Z0-9_+&*-]+)*@" + 
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                            "A-Z]{2,7}$"; 
                              
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 

    boolean is_duplicate(String username, String d_name, String email) throws IOException {
        String cmd = "is_duplicate "+username+" "+d_name+" "+email+"\n";
        con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
        String[] tokens = StringUtils.split(response);
        
        if(tokens[0].equals("exist"))
        {
            msg = tokens[1];
            return true;
        }
        else
        {
            return false;
        }
        
    }

    String getMsg() {
        return msg;
    }


    void register_this_user(String f_name, String l_name, String username, String d_name, String password1, String email) throws IOException {
        String cmd = "register_user "+f_name+" "+l_name+" "+username+" "+d_name+" "+password1+" "+email+"\n";
        con.serverOut.write(cmd.getBytes());
        InfoBox box = new InfoBox("User registered", "sd");
    }

    void send_complaint(String user_iden, String message) throws IOException {
        String cmd = "user_complaint "+user_iden+" "+message+"\n";
        con.serverOut.write(cmd.getBytes());
    }

    int getAccountLevel() {
        return Integer.parseInt(tokens[8]);
    }

                                                                                                                                                                                                                                                                                                        
}
