/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author vitol
 */
public class Group {
    public String title;
    public int id;
    public int isPublic;
    public String desc;
    public int creatorID;
    
   Standart_user user;
            
    public Group(String title, int isPublic,String desc,Standart_user user) {
        this.title = title;
        this.isPublic = isPublic;
        this.desc = desc;
        creatorID = user.getId();
        this.user = user;
    }
    public Group(String temp ,Standart_user user) throws IOException {
        this.user = user;
        getGroupInfo(temp);
    }
    public Group(Standart_user user) {
        this.user = user;
    }

    
    public boolean new_public_group() throws IOException
    {
        if(check_title_available_public_group())
        {
            logging_new_group(user.con.getServerin());
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean check_title_available_public_group() throws IOException {
        String cmd = "check_duplicate_title_p_group "+title+"\n";
        user.con.serverOut.write(cmd.getBytes());
        String response = user.con.bufferedIn.readLine();
        
        if(response.equalsIgnoreCase("nok"))
        {
            InfoBox box = new InfoBox("Public title already taken", "Sun is overrated");
            box = null;
            return false;
        }
        return true;
    }

    private void logging_new_group(InputStream inputStream) throws IOException {
        Time date = new Time();
        String cmd = title+" "+date.getDate()+" "+creatorID+" "+desc+"\n";

        user.con.serverOut.write(cmd.getBytes());
       
        BufferedReader readers = readers = new BufferedReader(new InputStreamReader(inputStream));
    }

    public void new_group() throws IOException {
        Time date = new Time();
        String cmd = "create_new_group "+title+" "+date.getDate()+" "+creatorID+" "+desc+"\n";
        user.con.serverOut.write(cmd.getBytes());

    }

    public String getAvailableGroups() throws IOException {
        String cmd = "not_a_member_of_public_group "+user.getId()+"\n";
        user.con.serverOut.write(cmd.getBytes());
        String response = user.con.bufferedIn.readLine();
        return response;
    }

    public void join_public_group(String title) throws IOException {
        String cmd = "join "+title+" "+user.getId()+"\n";
        user.con.serverOut.write(cmd.getBytes());
    }

    public String get_desc_from_public_group(String temp) throws IOException {
        String cmd = "desc "+temp+"\n";
        user.con.serverOut.write(cmd.getBytes());
        String response = user.con.bufferedIn.readLine();
        return response;
    }

    private void getGroupInfo(String temp) throws IOException {
        String cmd = "get_group_info "+temp+"\n";
        user.con.serverOut.write(cmd.getBytes());
        String response = user.con.bufferedIn.readLine();
        String[] tokens =StringUtils.split(response,null,4);
        this.title = tokens[0];
        creatorID = Integer.parseInt(tokens[1]);
        id = Integer.parseInt(tokens[2]);
        this.desc = tokens[3];
    }

    public void leaveGroup(int id_user, int id_group) throws IOException {
        String cmd = "leave_group "+id_user+" "+id_group+"\n";
        user.con.serverOut.write(cmd.getBytes());
        
    }

    public void deleteGroup(int id_group) throws IOException {
        String cmd = "delete_group "+id_group+"\n";
        user.con.serverOut.write(cmd.getBytes());
    }

    public void groupMembers() throws IOException {
        String cmd = "member_of_this_group "+id+" "+creatorID+"\n";
        user.con.serverOut.write(cmd.getBytes());
    }

    
    
    
}
