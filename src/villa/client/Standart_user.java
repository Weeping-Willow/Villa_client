/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villa.client;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;

/**
 
 * @author vitol
 */
public class Standart_user extends User{
    

    private final int id;
    private String fNmae;
    private String lName;
    private String displayName;
    private String email;
    
    public Standart_user(int id, String fName, String lName, String username, String displayName, String password,String email,Connect con) throws IOException {
        super(username, password, con);
        this.id = id;
        this.fNmae = fName;
        this.lName = lName;
        this.displayName = displayName;
        this.email = email;
    }

    public String member_of_groups() throws IOException {
        String cmd = "member_of "+id+"\n";
        con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
       
        return response;
    }
     public void logoff() throws IOException {
        String cmd = "quit\n";
        super.con.serverOut.write(cmd.getBytes());
        con.close();
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getId() {
        return id;
    }

    public boolean changeEmail(String tempPassword,String tempEmail) throws IOException {
           if(tempPassword.equals(password))
           {
               if(is_valid_email(tempEmail))
               {
                   String cmd = "change_email "+tempEmail+" "+username+"\n";
                   super.con.serverOut.write(cmd.getBytes());
                   String response = con.bufferedIn.readLine();
                   System.out.println(response);
                   if(response.equals("exists"))
                   {
                       InfoBox box = new InfoBox("Email already exists", "Ķeon");
                       return false;
                   }
                   else
                   {
                       System.out.println("das");
                       email = tempEmail;
                       return true;
                   }
               }
               else
               {
                   InfoBox box = new InfoBox("Not a valid email", "Ķeon");
                   return false;
               }
               
           }
           else
           {
               InfoBox box = new InfoBox("Password was wrong", "Ķeon");
               return false;
           }
    }

    public String getEmail() {
        return email;
    }

    public boolean change_display_name(String tempPassword, String tempDisplayName) throws IOException {
        if(tempPassword.equals(password))
           {
                String cmd = "change_display_name "+tempDisplayName+" "+username+"\n";
                super.con.serverOut.write(cmd.getBytes());
                String response = con.bufferedIn.readLine();
                if(response.equals("exists"))
                {
                    InfoBox box = new InfoBox("Display name already exists", "Ķeon");
                    return false;
                }
                else
                {
                    displayName = tempDisplayName;
                    return true;
                }
               }
           else
           {
               InfoBox box = new InfoBox("Password was wrong", "Ķeon");
               return false;
           }
    }

    public boolean change_password(String tempOldPassword, String tempNewPassword, String tempNewRePassword) throws IOException {
        if(tempOldPassword.equals(password))
           {
                 if(tempNewPassword.equals(tempNewRePassword))
               {
                   String cmd = "change_password "+tempNewPassword+" "+username+"\n";
                   super.con.serverOut.write(cmd.getBytes());
                   password = tempNewPassword;
                   return true;
               }
               else
               {
                   InfoBox box = new InfoBox("Passwords don't match", "Ķeon");
                   return false;
               }
               }
           else
           {
               InfoBox box = new InfoBox("Password was wrong", "Ķeon");
               return false;
           }
    }

    String getFirstName() {
        return fNmae;
    }

    String getLastName() {
        return lName;
    }

    public boolean change_basic(String tempFName, String tempLName, String tempPassword) throws IOException {
        if(!tempPassword.equals(password))
        {
            InfoBox box = new InfoBox("Password was wrong", "Ķeon");
            return false;
        }   
        else
        {
            String cmd = "change_basic "+tempFName+" "+tempLName+" "+username+"\n";
            super.con.serverOut.write(cmd.getBytes());
            fNmae = tempFName;
            lName = tempLName;
            return true;
        }
    }

    public String search_friend(String search_term) throws IOException {
        String cmd = "search_friends "+search_term+" "+id+"\n";
        super.con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
        return response;
    }

    public boolean send_new_friendship(String display_name) throws IOException {
        String cmd = "add_friend "+display_name+" "+id+"\n";
        super.con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
        if(response.equals("fail"))
        {
            InfoBox box = new InfoBox("error", "error");
            return false;
        }
        else
        {
            InfoBox box = new InfoBox("invite sent", "error");
            return true;
        }
    }

    public String get_pending_invite() throws IOException {
        String cmd = "get_pending_invite "+id+"\n";
        super.con.serverOut.write(cmd.getBytes());
        
        String response = con.bufferedIn.readLine();

        return response;
    }

    public void accept_friend(String selectedValue) throws IOException {
         String cmd = "accept_friend "+selectedValue+" "+id+"\n";
         super.con.serverOut.write(cmd.getBytes());
    }

    void deny_friend(String selectedValue) throws IOException {
        String cmd = "deny_friend "+selectedValue+" "+id+"\n";
        super.con.serverOut.write(cmd.getBytes());
    }

    String get_friend_list() throws IOException {
        String cmd = "get_friend_list "+id+"\n";
        con.serverOut.write(cmd.getBytes());
        String response = con.bufferedIn.readLine();
        
        return response;
    }

    void unfriend_friend(String selectedValue) throws IOException {
        String cmd = "unfriend_friend "+selectedValue+" "+id+"\n";
        super.con.serverOut.write(cmd.getBytes());
    }
    
}
