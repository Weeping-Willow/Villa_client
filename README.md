# Villa_client
Rvt 3d year 1st semester project 

Make a program in java that fits the following criteria in 3 weeks:

-Needs to have GUI for regular and admin user

-Needs to use sql db

-Needs to have registration and login system

-need to log the users actions

-Minimal class count is 7 excluding gui classes

-Need to use Arraylist

-Need to use 4 different java libaries

-For a higher grade you need to have inheritance

Refrences:

-https://www.youtube.com/playlist?list=PLdmXYkPMWIgCocLY-B4SvpQshQWC7Nc0C //to create the servers and client communication.And better understand threads

-https://www.youtube.com/watch?v=hDIXPbAODmE&t=1045s //understanding how to make a Gui on netbeans and db connections

project:
A chat app where you register as a user.Where you can create and join groups and have real time conversation.Add friends and unfriend them.

This is the the client side of the Project .It's setup to work in localhost atm with port 8818.

Server side of the app is available here:https://github.com/Weeping-Willow/Villa_Server

admin user:
Login:admin
Password:admin

Regular user:
Login:Kale
Password:Kale
